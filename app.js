// import express
const express = require("express");
const app = express();
app.use(express.json())

// import the list of profiles
let profilesList = require("./data.js")

function Where(arr, f)
{
	let result = []

	for (let i = 0; i < profilesList.length; i++)
	{
		if (f(arr[i])) result.push(arr[i])
	}

	return result
}

// a test endpoint - not part of the assessment
app.get("/", (req, res) =>
{
	res.status(200).send({msg:"Welcome to our page!"})
})

app.get("/api/profiles/:city?", (req, res) =>
{
	let city = req.params.city

	if (city === undefined)
	{
		res.send(profilesList)
	}
	else
	{
		res.send(Where(profilesList, profile => profile.city === city))
	}
})

app.post("/api/profiles", (req, res) =>
{
	profilesList.push(req.body)

	res.status(201).send({
		msg: "Created",
		inserted: req.body
	})
})

app.delete("/api/profiles", (req, res)=>
{
	res.status(501).send({msg: "Not Implemented"})
})

// anyone who imports this file will be able to use the "app" variable
module.exports = app